### These are the unmaintained [CRUX Linux](https://crux.nu/) ports moved from the [Yenjie](https://gitlab.com/SiFuh/yenjie) Repository.

This is also a working repository. If you wish to add it then download the [unmaintained.httpup](https://gitlab.com/SiFuh/unmaintained/-/raw/master/unmaintained.httpup) to **/etc/ports/** and edit the **/etc/prt-get.conf** by adding the line **prtdir /usr/ports/unmaintained** and then to update just run **ports -u unmaintained**. If you do not wish to add this repository and only want specific ports then you can download them separately instead. This is the recommended way.

This only exists for the benefit of those who wish to get ideas from the old Pkgfiles or any old patches and scripts that may come in handy.

---
![Alt text](https://gitlab.com/SiFuh/Documentation/-/raw/master/yenjie.png)

